GitLab user ID's:

Blagovest Tchasovnikarov: 2119758T Vesko
Kavisha Jayathunge: 2115370J
Borislav Gachev: 2140786G
Dafin Kozarev: 

SETUP GUIDE: Once you are in a fresh virtual environment, you will want to clone the project. To do this, go to the directory (in cmd) in which you wish to place the project and 
run the following command:

https://gitlab.com/jayathungek/SysRev.git

Afterwards, go to the directory, in which the .git directory is located (e-health). From there, go to Project. Now, you should install all of the packages that the 
project requires in order for the app to work properly. Enter the following command:

pip install -r requirements.txt

To finish the setup as well as to populate the database, please run the following commands:

1)python manage.py makemigrations 
2)python manage.py migrate 
3)python population_script.py

Now you can run the server by issuing the command python manage.py runserver. 

The population script has already created several users, which have some data in their profiles. They are as follows:

username: jill password: jill

username: bob password: bob

username: jen password: jen

username: theodor password: theodor

username: jim password: jim

That should be about everything you need to get the server up and running.