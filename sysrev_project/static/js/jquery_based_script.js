// Needed for the AJAX request timer
var timer;

// Function for building the query out of the selected option field and text fields
function display(){
	var textFields = $('.sb_line div');
	var result = "";
		
	result += textFields[0].children[1].value;
	for(i=1; i<textFields.length; i++){
		var text = textFields[i].children[1].value;
		if(text){
			result = "(" + result;
			result += ") "+textFields[i].children[0].value;
			result += " "+text;
		}
	};
	$('#txtOutput').val(result);
}

// Allows changing of results per page
// page -> current page
// maxPage -> maximum number of pages needed to display all results
// items -> how many items per page we have
// direction -> are we going forward, back, or junping to a page
function showResults(page, maxPage, items, newPage){
	$(".displayAbstract").hide();
	for(var i = 1; i<1+items; i++){
		$("#result_"+i*newPage).show();
	}
}

function suggest(){
	if($("#txtOutput").val()){
		query = $("#txtOutput").val();
		$.ajax({
			url: "../count_results/?query="+query,
			beforeSend: function(){
				$("#resultCount").html("<img src='../../static/images/ajax-loader.gif' alt='Loading results count'>");
			},
			success: function(data){
				$("#resultCount").html(data);
			},
			error: function(data){
				$("#resultCount").html("0");
				console.info(data);
			}
		});
		timer = null;
	}
}

$(document).ready(function() {
	
	// Needed for separating results in different pages
	var resultCount = $('.displayAbstract').length;	// How many results have we got?
	var perPage = 7;								// How many results per page do we want?
	var currentPage = 1;							// Which page are we on?
	var pageNeeded = Math.ceil(resultCount/perPage);// Number of pages

	
	// Needed for the advanced query buttons and builder
    var max_fields      = 20; //Maximum input boxes allowed
    var wrapper         = $(".sb_line"); //Fields wrapper
    var add_button      = $(".add_line"); //Add button class
   
    var searchBoxCount = 1;
	var boxId = 1;

	// Creates buttons for all pages in the footer
	for(var i=1; i-1<pageNeeded; i++){
		$("#tricky").append('<li><a href="#" class="pages">'+i+'</a></li>')
	};
	
	showResults(currentPage, pageNeeded, perPage, 1);

	
    $(add_button).click(function(e){ // On button click
        e.preventDefault();
        if(searchBoxCount < max_fields){
            searchBoxCount++;
			boxId++;

			var whatTo = '<div class="input_wrap">';
			whatTo += '<select id="fop_'+ boxId +'" class="op_combo"><option value="AND">AND</option><option value="OR">OR</option><option value="NOT">NOT</option></select>';
			whatTo += '<input id="fv_'+ boxId +'" size="50" class="list_term_text jig-ncbiautocomplete" autocomplete="off" submit="false" type="text">';
			whatTo += '<input type="button" class="rem_line btn" title="Remove this line from builder" value="Remove line" />';
			whatTo += '</div>';
    
            $(wrapper).append(whatTo);// Add new HTML objects
        }
    });
   
	// Remove the build line and update the query
    $(wrapper).on("click",".rem_line", function(e){
        e.preventDefault(); 
		$(this).parent('div').remove(); 
		searchBoxCount--; 
		display();
		suggest();
    });
	
	// Function for dynamically updating the query builder field
	$(wrapper).on("keyup", ".list_term_text", function(e){
		e.preventDefault();
		display();
		if(timer){
			clearTimeout(timer);
			timer = null;
		}
		timer = setTimeout(suggest,500);
	});
	
	
	// Toggles the Authors div in the results page
	$(".displayAbstract").on("click", ".toggleAuthors", function(e){
		e.preventDefault();
		LMNT = $(this).parent("div").children(".authors");
		LMNT.toggle();
	});
	
	$("#previous").click(function(e){
		if(currentPage>1){
			showResults(currentPage, pageNeeded, perPage, --currentPage);
		}
	});
	
	$("#next").click(function(e){
		if(currentPage<pageNeeded){
			showResults(currentPage, pageNeeded, perPage, ++currentPage);
		}
	});
	
	$(".pages").click(function(e){
		showResults(currentPage, pageNeeded, perPage, parseInt($(this).text()));
		currentPage=parseInt($(this).text());
		$(".pages").removeClass("active");
		$(this).addClass("active");
	});
	
	$("#tricky li a:first-child").addClass("active");	
	
});



