$(document).ready(function(){
	var pageNumber = 1; //counter which tracks on which page we are on
	var maxNumber = 10; //the id for the last page

	$("#previous").click(function(e){
		if(pageNumber == 1){
			$('#page'+pageNumber).toggle();
		}
		else{
			pageNumber--;
			$('#page'+pageNumber).toggle();
		}
		});

	$("#next").click(function(e){
		if(pageNumber == maxNumber){
			$('#page'+pageNumber).toggle();
		}
		else{
			pageNumber++;
			$('#page'+pageNumber).toggle();
		}
		
	});
	document.getElementById("reasonButton").disabled = true;
	$("results").click(function(){$('reasonButton').toggle();});

});