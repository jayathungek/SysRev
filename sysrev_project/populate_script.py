import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','sysrev_project.settings')

import django
django.setup()

from sysrev_app.models import UserProfile, User, Researcher
from django.core.files import File
from django.core.files.images import ImageFile
from django.contrib.staticfiles.templatetags.staticfiles import static

def populate():

    def addUser(username,email,password,website,organization):#profile_pic):
        u = User.objects.create_user(username,email,password)
        up = UserProfile.objects.get_or_create(user=u,website=website,organization=organization)[0]
        #pic = ImageFile(open(profile_pic,"r"))
        #up.profile_picture.save("jill.jpg",pic,save=True)
        return up

    def addResearcher(username,affiliation):#profile_pic):
        r = Researcher(researcher=User.objects.get(username=username),affiliation=affiliation)
        r.save()
        rp = Researcher.objects.get_or_create(researcher=r,affiliation=affiliation)
        #pic = ImageFile(open(profile_pic,"r"))
        #up.profile_picture.save("jill.jpg",pic,save=True)
        return rp

    user_jill = addUser(username='Jill', email='jill@gmail.com',
                           password='jill', website='https://www.facebook.com/',
                           organization='University of Cambridge')


    r_jill = addResearcher(username='Jill',affiliation='University of Cambridge')

    user_bob = addUser(username='Bob', email='bob@gmail.com',
                           password='bob', website='https://www.facebook.com/',
                           organization='Caledonian University')

    r_bob = addResearcher(username='Bob',affiliation='Caledonian University')

    user_jen = addUser(username='Jen', email='jen@gmail.com',
                           password='jen', website='https://www.facebook.com/',
                           organization='Harvard University')


    r_jen = addResearcher(username='Jen',affiliation='Harvard University')

    user_theodor = addUser(username='Theodor', email='theodor@gmail.com',
                           password='theodor', website='https://www.facebook.com/',
                           organization='University of Wolverhampton')

    r_theodor = addResearcher(username='Theodor',affiliation='University of Wolverhampton')

    user_jim = addUser(username='Jim', email='jim@gmail.com',
                           password='jim', website='https://www.facebook.com/',
                           organization='University of Strathclyde')

    r_jim = addResearcher(username='Jim',affiliation='University of Strathclyde')




# Start execution here!
if __name__ == '__main__':
    print "Starting Rango population script..."
    populate()