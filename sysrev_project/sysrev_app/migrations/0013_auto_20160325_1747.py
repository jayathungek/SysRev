# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sysrev_app', '0012_auto_20160325_1448'),
    ]

    operations = [
        migrations.AlterField(
            model_name='researcher',
            name='researcher',
            field=models.OneToOneField(to='sysrev_app.UserProfile'),
            preserve_default=True,
        ),
    ]
