# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sysrev_app', '0007_auto_20160321_0258'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='review',
            field=models.ForeignKey(default=60, to='sysrev_app.Review'),
            preserve_default=False,
        ),
    ]
