# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sysrev_app', '0006_auto_20160319_1708'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='link',
            field=models.CharField(default='ayy', max_length=1000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='organization',
            field=models.CharField(max_length=200, blank=True),
            preserve_default=True,
        ),
    ]
