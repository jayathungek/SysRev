# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sysrev_app', '0005_auto_20160319_1704'),
    ]

    operations = [
        migrations.AlterField(
            model_name='result',
            name='ID',
            field=models.CharField(max_length=10, serialize=False, primary_key=True),
        ),
    ]
