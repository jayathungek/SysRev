# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sysrev_app', '0004_userprofile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='result',
            name='id',
        ),
        migrations.AddField(
            model_name='result',
            name='ID',
            field=models.AutoField(default=1, serialize=False, primary_key=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='result',
            name='abstract',
            field=models.CharField(max_length=10000),
        ),
        migrations.AlterField(
            model_name='result',
            name='authors',
            field=models.CharField(max_length=10000),
        ),
        migrations.AlterField(
            model_name='result',
            name='title',
            field=models.CharField(max_length=1000),
        ),
    ]
