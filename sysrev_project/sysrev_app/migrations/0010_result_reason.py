# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sysrev_app', '0009_review_reviewtitle'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='reason',
            field=models.CharField(default='reason', max_length=10000),
            preserve_default=False,
        ),
    ]
