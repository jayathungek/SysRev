# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sysrev_app', '0010_result_reason'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='complete',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
