# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sysrev_app', '0002_auto_20160304_1728'),
    ]

    operations = [
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reviewID', models.IntegerField(unique=True)),
                ('researcher', models.ForeignKey(to='sysrev_app.Researcher')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='query',
            name='researcher',
        ),
        migrations.AddField(
            model_name='query',
            name='review',
            field=models.ForeignKey(default=1, to='sysrev_app.Review'),
            preserve_default=False,
        ),
    ]
