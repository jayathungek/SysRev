# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sysrev_app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='user',
        ),
        migrations.DeleteModel(
            name='UserProfile',
        ),
        migrations.AddField(
            model_name='query',
            name='researcher',
            field=models.ForeignKey(default=1, to='sysrev_app.Researcher'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='result',
            name='query',
            field=models.ForeignKey(default=1, to='sysrev_app.Query'),
            preserve_default=False,
        ),
    ]
