# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sysrev_app', '0008_result_review'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='reviewTitle',
            field=models.CharField(default='title', max_length=500),
            preserve_default=False,
        ),
    ]
