# -*- coding: utf-8 -*-

import json, ast , datetime, time, sys, urllib, urllib2
from Bio import Entrez, Medline
from urllib2 import HTTPError

reload(sys)
sys.setdefaultencoding('utf-8')

def search(search_terms):
    max_results = 50
    Entrez.email = "Leif.Azzopardi@glasgow.ac.uk"
    search_results = Entrez.read(Entrez.esearch(db="pubmed",
                                                term= search_terms,
                                                retmax=max_results,
                                                reldate=365, datetype="pdat",
                                                usehistory="n"))
    
    count = int(search_results["Count"])
    
    result_list = []
    attempt = 1
    while attempt <= 3:
        try:
            fetch_handle = Entrez.efetch(db="pubmed",rettype="medline",
                                         retmode="xml",retstart=0,
                                         retmax=max_results,
                                         webenv=search_results["WebEnv"],
                                         query_key=search_results["QueryKey"])
            attempt += 1

        except HTTPError as err:
            if 500 <= err.code <= 599:
                print("Received error from server %s" % err)
                print("Attempt %i of 3" % attempt)
                attempt += 1
                time.sleep(15)
            else:
                raise
                attempt +=1

    def toDate(dateDictionary):
        if len(dateDictionary) != 0:
            dateDictionary = dateDictionary[0]
            day = int(dateDictionary['Day'])
            month = int(dateDictionary['Month'])
            year = int(dateDictionary['Year'])
            date = datetime.date(year,month,day)
            
        else:
            date = datetime.date(1970,1,1)
        return date     
        
    def getAffiliation(author):
        if 'AffiliationInfo' in author.keys():
            if len(author['AffiliationInfo']) != 0:
                affiliation = author['AffiliationInfo'][0]['Affiliation']
                return affiliation
            else:
                return "No institution found"
        else:
            return "No institution found"


    def getAbstract(resultDictionary):
        if 'Abstract' in resultDictionary.keys():
            if len(resultDictionary['Abstract']) != 0:
                abstract = resultDictionary['Abstract']['AbstractText']
                return abstract
            else:
                return "No abstract found"
        else:
            return "No abstract found"

    def getName(author):
        if 'ForeName'in author.keys():
            forename = author['ForeName']
        else:
            forename = "No forename found"

        if 'LastName' in author.keys():
            lastname = author['LastName']
        else:
            lastname = "No lastname found"

        name = forename+" "+lastname
        return name

    def getAuthors(article):
        if 'AuthorList' in article.keys():
            authors = article['AuthorList']
            
        else:
            authors = []

        return authors
    
    print fetch_handle
    try:
        data = Entrez.read(fetch_handle)    
    except:
        return None
        
    for record in data:
        
        if 'MedlineCitation' in record.keys():
            ID = record['MedlineCitation']['PMID']
            title = record['MedlineCitation']['Article']['ArticleTitle']
            authors = getAuthors(record['MedlineCitation']['Article'])#['AuthorList']
            authors_list = []
            authorsString = ""

            if len(authors) != 0:
                for author in authors:
                    name = getName(author)
                    affiliation = getAffiliation(author)
                    info = name+"-"+affiliation
                    authors_list.append(info)

                for item in authors_list:
                    authorsString += "●"+item
            else:
                authorsString = "No authors found"
            

            pubDate = toDate(record['MedlineCitation']['Article']['ArticleDate'])

            abstract = getAbstract(record['MedlineCitation']['Article'])#['Abstract']['AbstractText']
            abstract_formatted = ""
            for section in abstract:
                abstract_formatted += section

            link = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=pubmed&id=%s&cmd=prlinks&retmode=ref"%(ID)

            result_list.append({
                'ID':ID,
                'Title':title,
                'Authors':authorsString,
                'Date':pubDate,
                'Abstract':abstract_formatted,
                'Link':link
                })
            
        
            fetch_handle.close()
        else:
            print "skipped"
    
    return result_list
