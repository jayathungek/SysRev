from Bio import Entrez
import urllib, urllib2

def rC(thingsToSearch):
        query = thingsToSearch.replace(" ", "+")
        
        Entrez.email = "Leif.Azzopardi@glasgow.ac.uk"
        searchResult = Entrez.read(Entrez.egquery(term = thingsToSearch, usehistory="y"))
        count = 0
        for entry in searchResult["eGQueryResult"]:
                if entry["DbName"]=="pubmed":
                        count = entry["Count"]
                        
        return count


