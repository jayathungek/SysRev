from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

class Researcher(models.Model):
    researcher = models.OneToOneField(User)
    affiliation = models.CharField(max_length=200)
    def __unicode__(self):
        return self.researcher.username

class Review(models.Model):
    reviewID = models.IntegerField(unique=True)
    reviewTitle = models.CharField(max_length = 500)
    researcher = models.ForeignKey(Researcher, on_delete=models.CASCADE)
    complete = models.BooleanField()
    def __unicode__(self):
        return str(self.reviewID)

class Query(models.Model):
    review =  models.ForeignKey(Review)
    Type = models.BooleanField() #novice/expert
    string = models.CharField(max_length=200)
    def __unicode__(self):
        return self.string

class Result(models.Model):
    review = models.ForeignKey(Review, on_delete=models.CASCADE)
    query = models.ForeignKey(Query, on_delete=models.CASCADE)
    ID = models.CharField(primary_key=True, max_length=10)
    authors = models.CharField(max_length=10000)
    title = models.CharField(max_length=1000)
    date = models.DateField()
    abstract = models.CharField(max_length=10000)
    link = models.CharField(max_length=1000)
    abstractApproved = models.BooleanField()
    documentApproved = models.BooleanField()
    reason = models.CharField(max_length = 10000)
    def __unicode__(self):
        return self.title

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)
    organization = models.CharField(max_length=200, blank=True)
    def __unicode__(self):
        return self.user.username
