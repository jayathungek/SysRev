from django.shortcuts import render, render_to_response
from sysrev_app.forms import UserForm, UserProfileForm, UpdateProfile, RevForm
from django.contrib.auth.models import User
from sysrev_app.models import UserProfile, Result, Query, Review, Researcher
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from sysrev_project.settings import MEDIA_URL
from sysrev_app.pubMed_search import search
from sysrev_app.search_count import rC
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def index(request):
    try:

        userid = User.objects.get(username=request.user)
        researcher = Researcher.objects.get(researcher=userid)
        reviews_list = Review.objects.all().filter(researcher=researcher)        
    
    except:
        reviews_list = []

    return render(request, 'sysrev_app/index.html',{'reviews_list':reviews_list})

def about(request):
    context_dict = {'bold': "This is an about page"}
    return render(request, 'sysrev_app/about.html', context_dict)



def register(request):
    registered = False
    print request.method
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            
            # Did the user provide a profile picture?
            # If so, we need to get it from the input form and put it in the UserProfile model.
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            profile.save()
            name = user_form.cleaned_data.get('username')
            u = Researcher(researcher=User.objects.get(username=name))
            u.save()
            registered = True
        else:
            print user_form.errors, profile_form.errors
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()
    return render(request,
            'sysrev_app/register.html',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered} )


try:
    LATEST_REV_ID = getattr(Review.objects.latest('reviewID'),'reviewID') # fetches the id of the latest review
except:
    LATEST_REV_ID = 1 #if there are no reviews in the database


def new_review(request):
    print "NEW REVIEW"
    global LATEST_REV_ID
    USER = request.user
    if USER.is_authenticated() == False:
        return user_login(request)
    else:
        rev_form = RevForm(request.POST)
        if request.method == 'POST':# and rev_form.is_valid():
            reqPost = True
            print "IS POST"
            if rev_form.is_valid():                
                userid = User.objects.get(username=request.user).id
                researcherPerson = Researcher.objects.get(researcher = userid)
                title = rev_form.cleaned_data['reviewTitle']
                LATEST_REV_ID += 1 #increments current latest to ensure unique ids
                r = Review(reviewID=LATEST_REV_ID,reviewTitle= title , researcher = researcherPerson, complete = False)
                r.save()
            else:
                reqPost = False
                print rev_form.errors
                       
            
        # elif request.method == "GET" :
        #     reqPost = True
        #     revID = int(request.GET['revID'])
        #     rev = Review.objects.get(reviewID=revID)
        #     q = Query.objects.get(review=rev)
        #     results = Result.objects.filter(review=rev,query=q,abstractApproved=True,documentApproved=True)

        else:            
            reqPost = False
            rev_form = RevForm()
        return render(request,
                'sysrev_app/create_review.html',
                {'rev_form': rev_form, 'reqPost': reqPost})


def build_query(request):
    try:
        global LATEST_REV_ID
        userid = User.objects.get(username=request.user).id
        researcherPerson = Researcher.objects.get(researcher = userid)
        r = Review.objects.all().filter(reviewID=LATEST_REV_ID, researcher = researcherPerson)
        return render(request, 'sysrev_app/new_review.html',{'revList':r})
    except:
        return register(request)

def edit_review(request):
    print "at edit page"
    try:
        userid = User.objects.get(username=request.user)
        researcher = Researcher.objects.get(researcher=userid)
        reviews_list = Review.objects.all().filter(researcher=researcher)        
    
    except:
        reviews_list = []

    return render(request, 'sysrev_app/edit_review.html',{'reviews_list':reviews_list})

def delete_review(request):
	try:
		u = User.objects.get(username=request.user)
		Researcher.objects.get(researcher=u.id)
		return render(request, 'sysrev_app/delete_review.html')
	except:
		return user_login(request)

def advanced(request):
    return render(request, 'sysrev_app/advanced.html')

def results(request):
    if request.method == 'POST': 
        query = request.POST.get('txtOutput')
        typeOfQuery = 1
        rev = Review.objects.get(reviewID=LATEST_REV_ID)
        q = Query(review=rev,Type=typeOfQuery,string=query)
        q.save()
        results = search(query)
        if results is not None:
            errors = False
            alreadyExists = False
            resultsFound = True
            
            for result in results:
                
                q = Query.objects.get(review=rev)
                i = result['ID']
                l = result['Link']
                a = result['Authors']
                t = result['Title']
                d = result['Date']
                ab = result['Abstract']                
                r = Result(review = rev,link=l,query=q,ID=i,authors=a,title=t,date=d,abstract=ab,abstractApproved=0,documentApproved=0)
                try:
                    r.save()
                except:
                    pass
                
        else:
            results = "Nothing to see here, move along!"
            errors = True
            resultsFound = False
            alreadyExists = False
    else:
        try:
            revID= int(request.GET['revID'])
            rev = Review.objects.get(reviewID=revID)
            q = Query.objects.get(review=rev)
            result_objects = Result.objects.all().filter(review=rev)
            results = []
            for result_object in result_objects:
                r_dict = {}
                r_dict['ID'] = result_object.ID
                r_dict['Authors'] = result_object.authors
                r_dict['Title'] = result_object.title
                r_dict['Date'] = result_object.date
                r_dict['Abstract'] = result_object.abstract
                r_dict['Link'] = result_object.link
                r_dict['abstractApproved'] = result_object.abstractApproved
                r_dict['documentApproved'] = result_object.documentApproved
                results.append(r_dict)
            alreadyExists = True
            resultsFound = True
            errors = False
            print rev
            #return render(request, 'sysrev_app/results.html', {'results' : results,  'errors' : errors ,'query':q,'review': rev, 'alreadyExists':alreadyExists,'resultsFound':resultsFound,'revID':revID})
        except:
            revID= int(request.GET['revID'])
            rev = Review.objects.get(reviewID=revID)
            q = Query.objects.get(review=rev)
            resultsFound = False
            results = "Nothing to see here, move along!"
            errors = True
            alreadyExists = True
            #return render(request, 'sysrev_app/results.html', {'results' : results,  'errors' : errors,'query':q,'review': rev, 'alreadyExists':alreadyExists,'resultsFound':resultsFound})
        
    return render(request, 'sysrev_app/results.html', {'results' : results,  'errors' : errors, 'query':q,'review': rev,'alreadyExists':alreadyExists,'resultsFound':resultsFound})

def docs(request):
    if request.method == 'POST':
        checked = request.POST.getlist('checks[]')
        revID_List = request.POST.getlist('queryID')
        rev = Review.objects.get(reviewID= revID_List[0])
        q = Query.objects.get(review=rev)
        
        if len(revID_List) > 0:
            revID = revID_List[0]
        else:
            results = "Nothing to see here, move along!"
            return render(request, 'sysrev_app/final_results.html',{'results' : results}) 

        revObj = Review.objects.get(reviewID=revID)
        revObj.complete = False
        rev = Review.objects.all().filter(reviewID=revID)
        # print rev
        que = Query.objects.all().filter(review=revObj)
        # print que
        result_list = Result.objects.filter(review=revObj,query=que)
        #print result_list
        num = 1
        for result in result_list:
            if str(num) in checked:
                result.abstractApproved = True
                result.save()
            else:
                result.abstractApproved = False
                result.save()
            num += 1
        results = Result.objects.filter(abstractApproved=True,query=que)
        print results

    else:
        results = "Nothing to see here, move along!"
        q = []
        rev = ["no review"]
    return render(request, 'sysrev_app/docs.html',{'results' : results, 'query': q,'review':rev[0]})


def final_results(request):
    if request.method == 'POST':
        checked = request.POST.getlist('checks[]')
        revID_List = request.POST.getlist('queryID')
        reasons_List = request.POST.getlist('reasons')
        print checked
        print reasons_List
        rev = Review.objects.get(reviewID= revID_List[0])
        q = Query.objects.get(review=rev)

        
        if len(revID_List) > 0:
            revID = revID_List[0]
        else:
            results = ["Nothing to see here, move along!"]
            return render(request, 'sysrev_app/final_results.html',{'results' : results ,'query': q,'review':rev[0]}) 

        # print checked
        # print revID
        revObj = Review.objects.get(reviewID=revID)
        revObj.complete = True        
        rev = Review.objects.all().filter(reviewID=revID)

        #print rev
        que = Query.objects.all().filter(review=revObj)
        # print que
        result_list = Result.objects.filter(review=revObj,query=que,abstractApproved=True)
        
        num = 1
        for result in result_list:
            if str(num) in checked:
                result.documentApproved = True                
                if str(num) in reasons_List:
                    result.reason = "Some Reason Here"
                result.save()
            else:
                result.documentApproved = False
                result.save()

            

            num += 1
        results = Result.objects.filter(abstractApproved= True ,documentApproved=True,query=que)
        print results

    else:

        results = ["Nothing to see here, move along!"]
        q = []
        rev = ["no review"]
    return render(request, 'sysrev_app/final_results.html',{'results' : results, 'query': q,'review':rev[0]})

def actual_final_results(request):

    if request.method == 'POST':
        checked = request.POST.getlist('checks[]')
        revID_List = request.POST.getlist('queryID')
        rev = Review.objects.get(reviewID= revID_List[0])
        rev.complete=True
        rev.save()
        q = Query.objects.get(review=rev)

        print revID_List
        if len(revID_List) > 0:
            revID = revID_List[0]
        else:
            results = ["Nothing to see here, move along!"]
            return render(request, 'sysrev_app/actual_final_results.html',{'results' : results ,'query': q,'review':rev[0]}) 

        # print checked
        # print revID
        rev  = Review.objects.get(reviewID=revID)
        rev.complete = True        
        
        que = Query.objects.all().filter(review=rev)
        # print que
        results = Result.objects.filter(review=rev,query=que,abstractApproved=True,documentApproved=True)
        
    else:
        # try:
        revID= int(request.GET['revID'])
        rev = Review.objects.get(reviewID=revID)
        q = Query.objects.get(review=rev)
        results = Result.objects.filter(review=rev,query=q,abstractApproved=True,documentApproved=True)
        # except:    
        #     results = ["Nothing to see here, move along!"]
        #     q = []
        #     rev = ["no review"]
    return render(request, 'sysrev_app/actual_final_results.html',{'results' : results, 'query': q,'review':rev})

def profile_page(request):
	context_dict = {}
	if request.user.is_authenticated():
		context_dict['username'] = request.user.username
		context_dict['email'] = request.user.email
		context_dict['pic'] =  request.user.userprofile.picture
		context_dict['website'] = request.user.userprofile.website
		context_dict['organisation'] = request.user.userprofile.organization
		context_dict['med'] = MEDIA_URL
	else:
		return render(request, 'sysrev_app/')
	return render(request, 'sysrev_app/profile_page.html', context_dict)

@login_required
def update_profile(request):
     # Request the context.
	context = RequestContext(request)
	context_dict = {}
	old_user=User.objects.get(username=request.user)
	old_profile = UserProfile.objects.get(user=request.user)
    # If HTTP POST, we wish to process form data and update the account.
	if request.method == 'POST':
		profile_form = UserProfileForm(data=request.POST)
		user_form = UserForm(data=request.POST)
		# A valid form?
		if profile_form.is_valid():
			
            # Update the password if a new password has been typed
			lastHope = User.objects.get(username=request.user)
			if request.POST.get("password", False):
				lastHope.set_password(request.POST["password"])
			# same for email
			if request.POST.get("email", False):
				lastHope.email=request.POST["email"]
				
			lastHope.save();
			# and update the rest of the stuff
			UserProfile.objects.filter(user=request.user).update(website=request.POST["website"])
			UserProfile.objects.filter(user=request.user).update(organization=request.POST["organization"])
			
            # Profile picture supplied? If so, we put it in the new UserProfile.
			if request.FILES.get('picture', False):
				UserProfile.objects.filter(username=request.user).update(picture=request.FILES['picture'])
			
			return render(request, 'sysrev_app/profile_page.html')


        # Invalid form(s) - just print errors to the terminal.
		else:
			return render(request, 'sysrev_app/edit_profile.html')
	else:
		profile_form=UserProfileForm(instance = old_profile)
		user_form=UserForm(instance = old_user)
		
	context_dict['profile_form']= profile_form
	context_dict['user_form'] = user_form
    # Render and return!
	return render(request, 'sysrev_app/edit_profile.html', context_dict)



def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/sysrev_app/')
            else:
                return HttpResponse("Your account is disabled.")
        else:
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")

    else:
        return render(request, 'sysrev_app/login.html', {})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/sysrev_app/')
	
def expected_results(request):
	
	query = None
	if request.method=='GET':
		query = request.GET.get('query', False)
		if query:
			return HttpResponse(rC(query))
		else:
			return HttpResponse("No query submitted.")
	else:
		return HttpResponseRedirect('sysrev_app/advanced')
