from django.conf.urls import patterns, url
from sysrev_app import views

urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),
		url(r'^index', views.index, name='index'),
        url(r'^about', views.about, name='about'),
        url(r'^new_review', views.build_query, name='new_review'),
        url(r'^edit_review', views.edit_review, name='edit_review'),
        url(r'^delete_review', views.delete_review, name='delete_review'),
        url(r'^advanced/$', views.advanced, name='advanced'),
        url(r'^profile_page/$', views.profile_page, name='profile_page'),
        url(r'^results/$', views.results, name='results'),
        url(r'^docs/$', views.docs, name='docs'),
        url(r'^register/$', views.register, name='register'),
        url(r'^login/$', views.user_login, name='login'),
        url(r'^logout/$', views.user_logout, name='logout'),
        url(r'^edit_profile/$', views.update_profile, name='edit_profile'),
        url(r'^create_review/$', views.new_review, name='create_review'),
        url(r'^final_results/$', views.final_results, name='final_results'),
        url(r'^actual_final_results/$', views.actual_final_results, name='actual_final_results'),
        url(r'^count_results/$', views.expected_results, name='count_results'),
        )
