from django.contrib import admin
from sysrev_app.models import Researcher, Query, Result, UserProfile, Review

admin.site.register(UserProfile)
admin.site.register(Researcher)
admin.site.register(Query)
admin.site.register(Result)
admin.site.register(Review)